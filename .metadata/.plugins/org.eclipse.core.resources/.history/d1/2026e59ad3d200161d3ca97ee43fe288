/***************************************************************************************
 * File		: MAIN.C
 * Program	: PRU MCP3202 Reader
 * Author	: Bryan Wilcutt
 *
 * Description:
 *
 * Reads 12-bit ADC values from the Microchip MCP3202 A/D Convertor
 *
 * Pins used by this application:
 *
 * p9_28 - SPI1_CS0  acts as MCP3202 CS Pin #1
 * p9_29 - SPI1_D0   acts as MOSI Input, MCP3202 DIN Pin #5
 * p9_30 - SPI1_D1   acts as MISO Output, MCP3202 DOUT Pin #6
 * p9_31 - SPI1_SCLK acts as MCP3202 CLK Pin #7
 * p9_1  - GND       acts as MCP3202 VSS Pin #4
 * P9_4  - +3.3V     acts as MCP3202 VDD/VREF Pin #8
 *
 * Recommended OCP DeviceTree Entry:
 *
 *  mcp3202_pins: mcp3202_pins {
 *   pinctrl-single,pins = <
 *             0x19c (PIN_OUTPUT_PULLDOWN | MUX_MODE5)	// MCP3202 Pin #1 CS     -> P9_28 = pr1_pru0_pru_r30_3
 *             0x194 (PIN_INPUT_PULLDOWN  | MUX_MODE6)  // MCP3202 Pin #5 DIN    -> P9_29 = pr1_pru0_pru_r31_1
 *             0x198 (PIN_OUTPUT_PULLDOWN | MUX_MODE5)  // MCP3202 Pin #6 DOUT   -> P9_30 = pr1_pru0_pru_r30_2
 *             0x190 (PIN_OUTPUT_PULLDOWN | MUX_MODE5)  // MCP3202 Pin #7 CLK    -> P9_31 = pr1_pru0_pru_r30_0
 *   >;
 * };
 *
 * Usage:
 *
 * Once this app begins, data may be pulled via the built in queue.  The queue is a round
 * robin mechanism that the OUI driver polls for data.   When an external entity pulls data
 * from the queue mechanism, it must first set the pActiveFlag to 0 and wait for the
 * pRunningFlag to become 0.  This will assure the queue mechanism is available for dequeue
 * operations.  Upon completion of dequeing, setting the pActiveFlag to 1 will continue
 * the application.  The application, at that point, will set the pRunningFlag to 1 to
 * indicate that it is operating normally.
 * 
 * To change the sample rate, first change the appropriate value in the sharedMem
 * and then set pRefresh to 1.  When pRefresh is set back to 0, the SPI has been
 * reprogrammed.   Note that all queued data is lost during this process.
 **************************************************************************************/

#include "cmperCfg.h"
#include "mcspiCfg.h"
#include <stdint.h>
#include <pru_cfg.h>
#include <pru_intc.h>
#include <pru_ctrl.h>
#include "resource_table.h"
#include "spiAPI.h"
#include "mcp3202.h"

#ifndef PRU_SRAM
#define PRU_SRAM __far __attribute__((cregister("PRU_SHAREDMEM", near)))
#endif

volatile register uint16_t __R31; // Input pins
volatile register uint16_t __R30; // Output pins

// Function Prototypes

void init_spi(void);
void init_data(void);
uint16_t get_value(uint8_t mosiword);
void cycleleds();
void sampleAverage(uint16_t ch1, uint16_t ch2);

// Global references

volatile __far uint32_t CT_DDR		__attribute__((cregister("DDR",   near), peripheral));
volatile __far pruIntc CT_INTC 		__attribute__((cregister("PRU_INTC", far), peripheral));
volatile __far pruCfg CT_CFG 		__attribute__((cregister("PRU_CFG", far), peripheral));
volatile cmperCfg_s *perCfg = ((volatile cmperCfg_s *)(CM_PER_BASE));

PRU_SRAM volatile __far ctrlData_s ctrlData;

/**************************************************************
 * Function 	: main
 * Input		: None
 * Output		: Int (ignore)
 * Description	:
 *
 * Enables the PRU, initializes the SPI and begins reading
 * from the MCP3202 immediately.
 **************************************************************/
int main(void)
{
	uint16_t ch0 = 0;
	uint16_t ch1 = 0;

	// Enable OCP Master Port --
	// The PRUs have access to all resources on the SoC through the Interface/OCP Master port, and the
	// external host processors can access the PRU-ICSS resources through the Interface/OCP Slave port. (sec 4.1)

	CT_CFG.SYSCFG_bit.STANDBY_INIT = 0;

    // C28 defaults to 0x00000000, we need to set bits 23:8 to 0x0100 in order to have it point to 0x00010000

	PRU0_CTRL.CTPPR0_bit.C28_BLK_POINTER = 0x0100;

    // Initialize configuration and data stacks

    init_data();

	// Initialize SPI Port for our use

	init_spi();

	ctrlData.magic = MCP3202_MAGIC_NUMBER;

	while (1)
	{
		if (ctrlData.readyFlag)
		{
			if (1 == ctrlData.pConfigFlag)
			{
				// Check for incoming msg from Host.  This would pertain to changing the configuration
				// parameters of the SPI.

				ctrlData.pConfigFlag = 0;

				// Reset SPI

				init_spi();
			} else {
				// Read MCP3202 Data and store to virtqueue for host to handle

				ch0 = get_value(ctrlData.mosiWordOut_ch0) & 0xfff;
				ch1 = get_value(ctrlData.mosiWordOut_ch1) & 0xfff;

				sampleAverage(ch0, ch1);
			}
		}
	}

	// Technically, we never get here but we'll put in ending behavior to be nice-nice.

	__halt();
	
	return 0;
}

/**************************************************************
 * Function 	: get_value
 * Input		: MCP3202 Configuation value (4 bits)
 * Output		: uint16_t value (12-bits)
 * Description	:
 *
 * Executes the algorithm for physically reading from the
 * MCP3202 via the SPI.  The MCP config is written (4 bits)
 * followed by reading 12 bits from the MCP.  Data is
 * returned as a 16 bit value although bits 0-11 are
 * relevant.  See figure 5-1, pp 15, MCP3202 spec.
 **************************************************************/
uint16_t get_value(uint8_t mosiout)
{
	uint16_t outVal = 0;
	uint32_t spiReg = 0;
	uint16_t retVal = 0;

	spiReg = (0 == ctrlData.spiPort) ? SOC_SPI_0_REGS : SOC_SPI_1_REGS;

	// Enable the channel when we're going to write data, disable it afterwords so another channel
	// may be used.  Only 1 channel is available at a time.  See Sec. 24.3.2.6.1, pp 4792.

	SPI_ChannelSet(spiReg, SPI_CHANNEL_ENABLE, ctrlData.spiCh);

	// MCP3202 is 24-bit access to get 12-bits of data.  Wirte 00000001b to the port as the first byte.

	SPI_TransmitData8(spiReg, lockin, ctrlData.spiCh);

	// Write data, 4 bits (or WL setting).

	outVal = mosiout;
	outVal <<= (16 - ctrlData.mosiWordLen);

	SPI_TransmitData16(spiReg, outVal, ctrlData.spiCh);

	while (!Is_RxDataReady(spiReg, ctrlData.spiCh));

	retVal = SPI_ReceiveData16(spiReg, ctrlData.spiCh);

	// Deactivate SPI channel.

	SPI_ChannelSet(spiReg, SPI_CHANNEL_DISABLE, ctrlData.spiCh);

	return retVal;
}

/**************************************************************
 * Function 	: init_spi
 * Input		: none
 * Output		: none
 * Description	:
 *
 * Initialises the output queue to an empty state and changes
 * the SPI clock rate to match the indicated sample rate
 * via pSampleRate.
 **************************************************************/
void init_spi(void)
{
	uint32_t spiReg = 0;
	uint32_t sampleRate = 0;

    /* Access PRCM (without CT) to initialize SPI_0 clock.  This must be done before the SPI hardware
     * can be used.  Without doing so, SPI_Reset will lock up.
     */
	perCfg->cm_per_spi1_clkctrl_bit.modulemode = ON;

	spiReg = (0 == ctrlData.spiPort) ? SOC_SPI_0_REGS : SOC_SPI_1_REGS;

	// Set up SPI IC for SPIx, Channel 1.

	SPI_Reset(spiReg);
	SPI_CSSet(spiReg, SPI_CS_ENABLE);
	SPI_MasterModeSet(spiReg, SPI_MULTI_CH, SPI_TX_RX_MODE, SPI_CHCONF_DPE0_DISABLED | SPI_CHCONF_DPE1_ENABLED | SPI_CHCONF_IS_LINE0, ctrlData.spiCh);
	SPI_MasterModeEnable(spiReg);

	// TODO: Rates should be programmable in the future
	// Clock is 48MHZ
	// Sample rate is 20kSPS * 16 bit samples = 320
	// CLK rate to MCP3202 should be: 48MHZ / 320 = 150 ratio

	sampleRate = (16 * ctrlData.pSampleRate);
	SPI_ClkConfig(spiReg, RATE_48MHZ, sampleRate, ctrlData.spiCh, ctrlData.spiClkMode); // Clk is 48MHZ, we need 100kSPS @ 16 bits given 100000/16

	SPI_WordLengthSet(spiReg, SPI_WORD_LENGTH(8), ctrlData.spiCh); // bww 1.4.17 Word length was 8
	SPI_WordCountSet(spiReg, 3);								   // bww 1.4.17 was 1 not 3

	SPI_CSPolaritySet(spiReg, SPI_CS_POL_LOW, ctrlData.spiCh);
	// bww changed enable to disable
	SPI_TxFIFODisable(spiReg, ctrlData.spiCh);
	SPI_RxFIFODisable(spiReg, ctrlData.spiCh);
	SPI_DataPinDirection(spiReg, SPI_D1_OUT_D0_IN); // D0 Input, D1 Output

	if (ctrlData.spiEnableSB)
	{
		SPI_StartBitEnable(spiReg, ctrlData.spiCh);
		SPI_StartBitPolaritySet(spiReg, SPI_CHCONF_SBPOL_LOWLEVEL, ctrlData.spiCh);
	} else {
		SPI_StartBitDisable(spiReg, ctrlData.spiCh);
	}
}

/**************************************************************
 * Function 	: sampleAverage
 * Input		: uint16_t ADC value for channel 1
 * Input		: uint16_t ADC value for channel 2
 * Output		: None
 * Description	:
 *
 * Pushes new value onto round robin queue.
 **************************************************************/
void sampleAverage(uint16_t ch1v, uint16_t ch2v)
{
	// Lock stack

	ctrlData.ch1avg += ch1v;
	ctrlData.ch2avg += ch2v;
	ctrlData.avgWindow++;

	if (ctrlData.avgWindow >= ctrlData.pAvgWindow)
	{
		ctrlData.avgWindow = 0;

		// Lock ring buffer
		// New item added to head.  Tail is bumped and lost if buffer is full.

		while (ctrlData.stackLock) __delay_cycles(100);
		ctrlData.stackLock = 1;

		ctrlData.stack[ctrlData.stackHead].ch1 = ctrlData.ch1avg / ctrlData.pAvgWindow;
		ctrlData.stack[ctrlData.stackHead].ch2 = ctrlData.ch2avg / ctrlData.pAvgWindow;

		ctrlData.ch1avg = 0;
		ctrlData.ch2avg = 0;

		// Bump head

		ctrlData.stackHead++;
		ctrlData.stackCount++;

		if (ctrlData.stackCount > STACK_SIZE)
			ctrlData.stackCount = STACK_SIZE;

		if (ctrlData.stackHead >= STACK_SIZE)
			ctrlData.stackHead = 0; // Point back to the beginning.

		if (ctrlData.stackHead == ctrlData.stackTail) {
			// We're full, drop the tail and bump it.

			ctrlData.stackTail++;
			if (ctrlData.stackTail >= STACK_SIZE)
				ctrlData.stackTail = 0; // Point back to the beginning.
		}

		// Unlock stack

		if (ctrlData.stackLock)
			ctrlData.stackLock = 0;
	}
}

/**************************************************************
 * Function 	: init_data
 * Input		: None
 * Output		: None
 * Description	:
 *
 * Initializes internal structures.
 **************************************************************/
void init_data()
{
	int i;

	// Default configuration structure setup

	ctrlData.pInterval = DEFAULT_INTERVAL;
	ctrlData.pSampleRate = DEFAULT_SAMPLERATE;
	ctrlData.spiCh = DEFAULT_SPICH;
	ctrlData.spiClkMode = DEFAULT_SPICLKMODE;
	ctrlData.spiEnableSB = DEFAULT_SPISB;
	ctrlData.spiPort = DEFAULT_SPIPORT;
	ctrlData.mosiWordOut_ch0 = DEFAULT_MOSIBITSOUT_CH0;
	ctrlData.mosiWordOut_ch1 = DEFAULT_MOSIBITSOUT_CH1;
	ctrlData.mosiWordLen = DEFAULT_MOSIBITLEN;
	ctrlData.spiReadBits = DEFAULT_SPIREADBITS;
	ctrlData.pAvgWindow = DEFAULT_AVGWINDOW;
	ctrlData.readyFlag = DEFAULT_READYFLAG;
	ctrlData.pConfigFlag = 0;

	// Initialized data stacks

	ctrlData.stackHead = 0;
	ctrlData.stackTail = 0;
	ctrlData.stackCount = 0;
	ctrlData.stackLock = 0;
	ctrlData.ch1avg = 0;
	ctrlData.ch2avg = 0;
	ctrlData.avgWindow = 0;

	for (i = 0; i < STACK_SIZE; i++)
	{
		ctrlData.stack[i].ch1 = 0;
		ctrlData.stack[i].ch2 = 0;
	}
}

//**** Test function to cycle leds on and off
#if 0
static int ledstate = 0;
void cycleleds()
{
	if (ledstate == 0)
	{
		__R30 &= ~(1 << 14); // Turn pru1_pru0_pru_r30_14 led off (pin #12 on P8 header)
		//__delay_cycles(HALF_SECOND/2);
		ledstate = 1;
	} else {
		__R30 = (1 << 14);
		//__delay_cycles(HALF_SECOND/2);
		ledstate = 0;
	}
}
#endif
