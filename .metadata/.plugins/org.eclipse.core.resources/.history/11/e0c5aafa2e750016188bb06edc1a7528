#ifndef MCP3202_H
#define MCP3202_H

// App definitions

#define RATE_48MHZ 		((uint32_t)  48000000)
#define RATE_200MHZ		((uint32_t) 200000000)
#define MAX_SAMPLE_RATE ((uint32_t) 46500)
#define ON 				(0x2)
#define STACK_SIZE 		(256) // Number of samples storable, maximum size


// This data is shared with the user space and allows the following operations:
// 1. Refresh SPI with new sample rate
// 2. Turn PRU->MCP off so dequeue make take place
// 3. Stop PRU->MCP operations (idle noop)

#define MCP3202_MAGIC_NUMBER 0xD0C0FFEE

typedef struct __attribute__((__packed__)) {
	uint32_t	magic;
	uint16_t	readyFlag;
	uint16_t	mosiWordOut_ch0;
	uint16_t	mosiWordOut_ch1;
	uint16_t 	mosiWordLen;
	uint16_t	spiReadBits;

	uint16_t	pConfigFlag;	// 0 = Normal, 1 = New configuation available-- reset SPI
	uint16_t	pSampleRate;    // Samples per second
	uint16_t	pAvgWindow;		// Rolling average window size, must be ^2 (1, 2, 4, 8, 16, 32, 64, 128, 256)
	uint32_t	pInterval;		// Clks to wait between samples to get samples per second

	// SPI Configuration

	uint16_t	spiPort;
	uint16_t	spiCh;
	uint16_t	spiEnableSB;
	uint16_t	spiClkMode;

	uint16_t	stackLock;
	int16_t		stackCount;
	int16_t		stackHead;
	int16_t		stackTail;
	uint16_t	ch1avg;
	uint16_t	ch2avg;
	uint16_t	avgWindow;
	struct {
		uint16_t ch1, ch2;
	} stack[STACK_SIZE];
} ctrlData_s;

// Configuration bits for MCP3202
//
//                      Config bits          Channel Sel   Gnd
//                      SGL/DIFF  ODD/SIGN      0     1
//                      --------------------------------------
// Single-ended Mode        1        0          +     --    -
//                          1        1          --    +     -
// Pseudo-Diff Mode         0        0          IN+   IN-
//						    0        1          IN-   IN+

// Write our configuration to Single-Ended Mode
// 1  1  0  1
// ^  ^  ^  ^- MS BF, Begins MCP3202 to xmit 12 bits
// |  |  |---- ODD/SIGN to 0 (see above table)
// |  |------- SGL/DIFF to 1 (see above table)
// |-----------START bit to wake up MCP3202, Automatic with Am335x when StartBitEnable() is used
//
// NOTE: If start bit is turned on, don't turn it on in the MCP_MODE definition!!!

#define MCP_MODE_CH0 15 // 1101b  Start = 1. SGL = 1, ODD = 0, MSBF = 1
#define MCP_MODE_CH1 13 // 1111b  Start = 1, SGL = 1. ODD = 1, MSBF = 1

// Driver Default Configuration Information

#define DEFAULT_INTERVAL	(0)
#define DEFAULT_SAMPLERATE	(5000)
#define DEFAULT_SPICH		(0)
#define DEFAULT_SPICLKMODE	(0)
#define DEFAULT_SPISB		(0)
#define DEFAULT_SPIPORT		(1)
#define DEFAULT_MOSIBITSOUT_CH1	(MCP_MODE_CH1)
#define DEFAULT_MOSIBITSOUT_CH2 (MCP_MODE_CH2)
#define DEFAULT_MOSIBITLEN (4)
#define DEFAULT_SPIREADBITS	(13)
#define DEFAULT_READYFLAG	(1)

// Window size settings: (1 << n) where n is...
// 0 = 1 Sample
// 1 = 2 Samples
// 2 = 4 Samples
// 3 = 8 Samples
// 4 = 16 Samples, etc.

#define DEFAULT_AVGWINDOW	(4)

#endif
